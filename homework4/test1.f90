program test1
    use quadrature, only: error_table

    integer, dimension(7) :: nvals = (/5, 10, 20, 40, 80, 160, 320/)
    real :: a = 0.0
    real :: b = 2.0
    real :: int_true 

    int_true = (b-a) + (b**4 - a**4)/4.0

    print "('True value for the integral:', es22.14)", int_true

    call error_table(f1, a, b, nvals, int_true)

contains
real function f1(x)
    real, intent(in) :: x

    f1 = 1.0 + x**3
end function f1
end program test1



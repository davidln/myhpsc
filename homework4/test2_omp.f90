program test2
    use omp_lib
    use quadrature_omp, only: error_table

    integer, dimension(12) :: nvals 
    integer :: i
    real :: a = 0.0
    real :: b = 2.0
    real :: int_true 

    do i=1,size(nvals)
        nvals(i) = 5 * (2**i)
    enddo

    int_true = integral_f2(b) - integral_f2(a) 

    print "('True value for the integral:', es22.14)", int_true

    !$ call omp_set_num_threads(2)
    call error_table(f2, a, b, nvals, int_true)

contains

real function f2(x)
    real, intent(in) :: x
    f2 = 1.0 + x**3 + sin(1000*x)
end function f2

real function integral_f2(x)
    real, intent(in) :: x
    real :: k = 1000.0

    integral_f2 = x + 0.25*x**4 - (1.0/k)*cos(k*x)
end function
end program test2


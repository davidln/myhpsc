!this module compute the integral of a function with the trapezoid method

module quadrature
contains

real function trapezoid(f, a, b, n)
    ! Input:
    !   f -> funtion to integrate
    !   a -> left point
    !   b -> right point
    !   n -> number of points
    ! Returns:
    !   the estimate of the integral
    real, external :: f
    real, intent(in) ::  a, b
    integer, intent(in) :: n

    !local variables
    real :: trap_sum, xj, h
    integer :: j
    h = (b-a)/(n-1)

    trap_sum = 0.5*(f(a) + f(b))
    do j=2,n-1
        xj = a + (j-1)*h
        trap_sum = trap_sum + f(xj)
    end do

    trapezoid = h * trap_sum

end function trapezoid

subroutine error_table(f, a, b, nvals, int_true)
    real, external :: f     ! the function to integrate
    real, intent(in) :: a   ! the left point of the interval
    real, intent(in) :: b   ! the right point of the interval
    real, intent(in) :: int_true    ! the analytical value of the integral (real value)
    integer, dimension (:), intent(in) :: nvals ! array with several values of n to try

    integer :: i,n
    real :: int_trap, error, last_error = 0.0, ratio
    print *, "    n         trapezoid            error       ratio"
    n = size(nvals)
    do i=1,n
        int_trap = trapezoid(f, a, b, nvals(i))
        error = abs(int_trap - int_true)
        ratio = last_error / error
        last_error = error
        print "(i8, es22.14, es13.3, es13.3)", nvals(i), int_trap, error, ratio
    end do
end subroutine error_table

end module quadrature

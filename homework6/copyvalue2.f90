program copyvalue2
    use mpi
    implicit none
    integer :: ierr, numprocs, proc_num, val
    integer, dimension(MPI_STATUS_SIZE) :: status

    call mpi_init(ierr)
    call mpi_comm_size(MPI_COMM_WORLD, numprocs, ierr)
    call mpi_comm_rank(MPI_COMM_WORLD, proc_num, ierr)

    if (numprocs == 1) then
        print *, "Only one process, cannot do anything"
        call mpi_finalize(ierr)
        stop
    endif

    if (proc_num == numprocs - 1) then
        val = 55
        print "('Process: ', i3, ' sends    value: ', i3)", proc_num, val
        call mpi_send(val, 1, MPI_INTEGER, proc_num-1, 21, MPI_COMM_WORLD, ierr)
    end if

    if (proc_num > 0 .and. proc_num < numprocs - 1) then
        call mpi_recv(val, 1, MPI_INTEGER, proc_num+1, 21, MPI_COMM_WORLD, status, ierr)
        print "('Process: ', i3, ' receives value: ', i3)", proc_num, val
        val = val -1
        call mpi_send(val, 1, MPI_INTEGER, proc_num-1, 21, MPI_COMM_WORLD, ierr)
        print "('Process: ', i3, ' sends    value: ', i3)", proc_num, val
    end if

    if (proc_num == 0) then
        call mpi_recv(val, 1, MPI_INTEGER, proc_num+1, 21, MPI_COMM_WORLD, status, ierr)
        print "('Process: ', i3, ' receives value: ', i3)", proc_num, val
    end if

    call mpi_finalize(ierr)
endprogram copyvalue2

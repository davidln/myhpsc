def solve(ffpx, x0, debug = False) :
	"""
	Estimate the zero of f(x) using Newton's method.
	Input:
		ffp -> it is a function to compute the f(x) and fp(x) first derivate of f(x)
		x0 -> initial guess for Newton method
		debug -> if True switch on the debug mode
	Return value:
		(x, n_iter) -> a tuple with the approximation of f(x) and the number of iterations
	"""

	#max iterations for the algorythm
	maxiter = 20
	#tolerance
	tol = 1e-14

	x = x0
	if debug :
		print "Initial guess: x = %22.15e" % x

	for i in range(1, maxiter+1) :
		fx, fpx = ffpx(x)
		if abs(fx) < tol :
			break

		deltax = fx/fpx
		x = x - deltax

		if debug :
			print "After %i iterations, x = %22.15e" % (i, x)

	if i == maxiter :
		fx, fpx = ffpx(x)
		if abs(fx) > tol :
			print "*** Warning: has not yet converted"

	n_iter = i-1

	return (x, n_iter)

def fvals_sqrt(x):
    """
    Return f(x) and f'(x) for applying Newton to find a square root.
    """
    f = x**2 - 4.
    fp = 2.*x
    return f, fp

def test1(debug_solve=False):
    """
    Test Newton iteration for the square root with different initial
    conditions.
    """
    from numpy import sqrt
    for x0 in [1., 2., 100.]:
        print " "  # blank line
        x,iters = solve(fvals_sqrt, x0, debug=debug_solve)
        print "solve returns x = %22.15e after %i iterations " % (x,iters)
        fx,fpx = fvals_sqrt(x)
        print "the value of f(x) is %22.15e" % fx
        assert abs(x-2.) < 1e-14, "*** Unexpected result: x = %22.15e"  % x

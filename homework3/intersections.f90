! $UWHPSC/codes/fortran/newton/test1.f90

program intersections

    use newton, only: solve
    use functions, only: g1, g2, gp1, gp2, g, gp

    implicit none
    real(kind=8) :: x, x0, fx
    real(kind=8) :: x0vals(4)
    integer :: iters, itest
    logical :: debug         ! set to .true. or .false.

    debug = .false.

    ! values to test as x0:
    x0vals = (/-2.2d+0, -1.6d+0, -0.8d+0, 1.4d+0/)

    do itest=1,4
        x0 = x0vals(itest)
        print *, ' '  ! blank line
        call solve(g, gp, x0, x, iters, debug)

        print 10, x0
10      format('With initial guess x0 = ', es22.15, ',')
        print 11, x, iters
11      format('	solve returns x = ', es22.15, ' after', i3, ' iterations')
        print 12, g1(x), g2(x)
12      format('	g1(x) = ', es22.15, ' g2(x) = ', es22.15)


        enddo

end program intersections

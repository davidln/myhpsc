! $UWHPSC/codes/fortran/newton/functions.f90

module functions

implicit none
real(kind=8), parameter :: pi = acos(-1.d0)
contains

real(kind=8) function f_sqrt(x)
    implicit none
    real(kind=8), intent(in) :: x

    f_sqrt = x**2 - 4.d0

end function f_sqrt


real(kind=8) function fprime_sqrt(x)
    implicit none
    real(kind=8), intent(in) :: x
    
    fprime_sqrt = 2.d0 * x

end function fprime_sqrt

real(kind=8) function g1(x)
    implicit none
    real(kind=8), intent(in) :: x

    g1 = x * cos(pi*x)

end function g1


real(kind=8) function gp1(x)
    implicit none
    real(kind=8), intent(in) :: x
    
    gp1 = cos(pi*x) - x*pi*sin(pi*x) 

end function gp1

real(kind=8) function g2(x)
    implicit none
    real(kind=8), intent(in) :: x

    g2 = 1.0d+0 - 0.6d+0*x**2

end function g2


real(kind=8) function gp2(x)
    implicit none
    real(kind=8), intent(in) :: x
    
    gp2 = -1.2d+0*x

end function gp2

real(kind=8) function g(x)
    implicit none
    real(kind=8), intent(in) :: x

    g = g1(x) - g2(x)

end function g

real(kind=8) function gp(x)
    implicit none
    real(kind=8), intent(in) :: x
    
    gp = gp1(x) - gp2(x)

end function gp

end module functions

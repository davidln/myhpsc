module init
	implicit none
contains
subroutine set_epsilon(ep)
	use functions, only: eps
	implicit none
	real(kind=8), intent(in) :: ep 
	eps = ep
end subroutine set_epsilon

subroutine set_tolerance(tolerance)
	use newton, only: tol
	implicit none
	real(kind=8), intent(in) :: tolerance
	tol = tolerance
end subroutine set_tolerance
end module init

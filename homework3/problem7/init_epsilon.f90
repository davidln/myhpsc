subroutine set_epsilon(ep)
	implicit none
	real(kind=8), intent(in) :: ep 
	use functions, only: eps
	eps = ep
end subroutine set_epsilon

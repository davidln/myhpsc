program test_quartic

    use newton, only: solve, tol
    use functions, only: f_quartic, fprime_quartic, quartic_root, eps
    use init, only: set_epsilon, set_tolerance

    implicit none
    real(kind=8) :: x, x0 = 4.d0, fx
    real(kind=8) :: epsilons(3) = (/1.d-4, 1.d-8, 1.d-12/)
    real(kind=8) :: xstar !quartic root analytical method
    real(kind=8) :: tolerances(3) = (/1.d-5, 1.d-10, 1.d-14/)
    integer :: iters, ep, to
    logical :: debug = .false.         ! set to .true. or .false.

    print "('Starting with initial guess ', es24.15)", x0
    print *, '    epsilon        tol    iters          x                 f(x)        x-xstar'
    do ep=1,3
        call set_epsilon(epsilons(ep))
        xstar = quartic_root(epsilons(ep))
        do to=1,3
            call set_tolerance(tolerances(to))
            call solve(f_quartic, fprime_quartic, x0, x, iters, debug)
            fx = f_quartic(x)
            print "(2es13.3, i4, es24.15, 2es13.3)", epsilons(ep), tolerances(to), iters, x, fx, x-xstar
!11 format(2es13.3, i4, es24.15, 2es13.3)
            !print "('x = ', es24.15, ' xstar = ', es24.15)", x, xstar
        end do
        print *, ''
    end do

end program test_quartic

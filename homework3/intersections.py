import numpy as np
import matplotlib.pyplot as plt
from newton import solve

def g1(x) :
	# first function
	return x * np.cos(np.pi*x)

def gp1(x) :
	# first function derivade
	return np.cos(np.pi*x) - x*np.pi*np.sin(np.pi*x)
	
def g2(x) :
	# second function
	return 1 - 0.6*x**2

def gp2(x) :
	# second function derivade
	return -1.2*x

def function(x) :
	# return the tuple: (f(x), f'(x)) where
	# f(x) = g1(x) - g2(x)
	# f'(x) = df(x)/dx
	return ( g1(x) - g2(x), gp1(x) - gp2(x) )

X0 = [-2.2, -1.6, -0.8, 1.4]
x_intersec = []
fx_intersec = []
for x0 in X0 :
	print ""
	print "With initial guess x0 = %22.15e" % x0
	(x, i) = solve(function, x0)
	print "\tsolve returns x = %22.15e after %i iterations" % (x, i)
	x_intersec.append(x)
	fx_intersec.append(g1(x))
	# for debugging. Let's check that g1(x) - g2(x) == 0
	print "\tg1(x) = %22.15e g2(x) = %22.15e" % (g1(x), g2(x))

#now plot the two functions and the intersections
x_plot = np.linspace(-5, 5, 10000)
y1_plot = np.array( map(g1, x_plot) )
y2_plot = np.array( map(g2, x_plot) )

plt.figure(1)
plt.clf()
l1 = 'g1(x) = x*cos(pi*x)'
l2 = 'g2(x) = 1 - 0.6*x^2'
p1 = plt.plot(x_plot, y1_plot, 'r-', label=l1)
p2 = plt.plot(x_plot, y2_plot, 'b-', label=l2)
plt.plot(x_intersec, fx_intersec, 'ko')
plt.legend((l1, l2), loc = 'best')
plt.savefig('intersections.png')

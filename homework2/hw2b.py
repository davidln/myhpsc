
"""
Demonstration module for quadratic interpolation.
Modified by: David Lorenzo
"""


import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import solve

def quad_interp(xi,yi):
    """
    Quadratic interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1,2.
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2.

    """

    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have length 3"
    assert len(xi)==3 and len(yi)==3, error_message

    # Set up linear system to interpolate through data points:
    #A = np.array([[1., xi[0], xi[0]**2], [1., xi[1], xi[1]**2], [1., xi[2], xi[2]**2]])
    A = np.vstack([np.ones(3), xi, xi**2]).T

    ### Fill in this part to compute c ###
    c = solve(A, yi)

    return c

def plot_quad(xi, yi):
    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have length 3"
    assert len(xi)==3 and len(yi)==3, error_message

    c = quad_interp(xi, yi)

    x = np.linspace(xi.min() -1, xi.max() + 1, 1000)
    y = c[0] + c[1]*x + c[2]*x**2

    plt.figure(1)
    plt.clf()
    plt.plot(x, y, 'b-')

    plt.plot(xi, yi, 'ro')
    plt.ylim(y.min()-1, y.max()+1)
    plt.title("Data points and interpolating polynomial")
    plt.savefig("quadratic.png")

def cubic_interp(xi,yi):
    """
    Cubic interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1,2,3.
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2 + c[3]*x**3.

    """

    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have length 4"
    assert len(xi)==4 and len(yi)==4, error_message

    # Set up linear system to interpolate through data points:
    A = np.vstack([np.ones(4), xi, xi**2, xi**3]).T

    ### Fill in this part to compute c ###
    c = solve(A, yi)

    return c

def plot_cubic(xi, yi):
    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have length 3"
    assert len(xi)==4 and len(yi)==4, error_message

    c = cubic_interp(xi, yi)

    x = np.linspace(xi.min() -1, xi.max() + 1, 1000)
    y = c[0] + c[1]*x + c[2]*x**2 + c[3]*x**3

    plt.figure(1)
    plt.clf()
    plt.plot(x, y, 'b-')

    plt.plot(xi, yi, 'ro')
    plt.ylim(y.min()-1, y.max()+1)
    plt.title("Data points and interpolating polynomial")
    plt.savefig("cubic.png")

def pol_interp(xi,yi):
    """
    Polynomial interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1...n where n is the
    number of points
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2 + ... + c[n-1]*x**(n-1).

    """

    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have the same length"
    n = len(xi)
    assert len(yi)==n, error_message

    # Set up linear system to interpolate through data points:
    m = [ np.ones(n) ]
    for i in range(1, n, 1):
        m.append([xi**(i)])
    A = np.vstack(m).T

    ### Fill in this part to compute c ###
    c = solve(A, yi)

    return c

def plot_poly(xi, yi):
    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have the same length"
    n = len(xi)
    assert len(yi)==n, error_message

    c = pol_interp(xi, yi)

    x = np.linspace(xi.min() -1, xi.max() + 1, 1000)
    y = c[n-1]
    for j in range(n-1, 0, -1):
        y = y*x +c[j-1]

    plt.figure(1)
    plt.clf()
    plt.plot(x, y, 'b-')

    plt.plot(xi, yi, 'ro')
    plt.ylim(y.min()-1, y.max()+1)
    plt.title("Data points and interpolating polynomial")
    plt.savefig("poly.png")

def test_quad1():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([-1.,  0.,  2.])
    yi = np.array([ 1., -1.,  7.])
    c = quad_interp(xi,yi)
    print xi
    print yi
    c_true = np.array([-1.,  0.,  2.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)
        
def test_quad2():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([-2.,  3.,  5.])
    yi = np.array([ -18., -28., -74.])
    c = quad_interp(xi,yi)
    print xi
    print yi
    c_true = np.array([-4.,  1.,  -3.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)

def test_cubic1():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([-2., -1., 0.,  1.])
    yi = np.array([ -23., -2., 1, 10.])
    c = cubic_interp(xi,yi)
    print xi
    print yi
    c_true = np.array([1., 2., 3., 4.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)

def test_pol1():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([-2., -1., 0.,  1.])
    yi = np.array([ -23., -2., 1, 10.])
    c = pol_interp(xi,yi)
    print xi
    print yi
    c_true = np.array([1., 2., 3., 4.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)

def test_pol2():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([-2., -1., 0.,  1., 2.])
    yi = np.array([ 57., 3., 1, 15., 129.])
    c = pol_interp(xi,yi)
    print xi
    print yi
    c_true = np.array([1., 2., 3., 4., 5.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)

if __name__=="__main__":
    # "main program"
    # the code below is executed only if the module is executed at the command line,
    #    $ python demo2.py
    # or run from within Python, e.g. in IPython with
    #    In[ ]:  run demo2
    # not if the module is imported.
    print "Running tests..."
    print "\ntest_quad1"
    test_quad1()
    print "\ntest_quad2"
    test_quad2()
    print "\ntest_cubic1"
    test_cubic1()
    print "\ntest_pol1"
    test_pol1()
    print "\ntest_pol2"
    test_pol2()


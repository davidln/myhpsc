
module functions

    use omp_lib
    implicit none
    integer :: fevals(0:7)
    integer :: gevals(0:7)
    real(kind=8) :: c, d
    save

contains

real function g(x,y)
    real, intent(in) :: x, y
    integer thread_num

    thread_num = 0
    !$ thread_num = omp_get_thread_num()
    gevals(thread_num) = gevals(thread_num) + 1

    g = sin(x + y)
end function g

    real(kind=8) function f(x)
        implicit none
        real(kind=8), intent(in) :: x 
        integer thread_num
        real :: trap_sum, h, yj
        integer :: ny = 1000
        integer :: j

        ! keep track of number of function evaluations by
        ! each thread:
        thread_num = 0   ! serial mode
        !$ thread_num = omp_get_thread_num()
        fevals(thread_num) = fevals(thread_num) + 1
        
        h = (d-c)/(ny-1)
        f = 0.5*(g(x, d) + g(x, c))

        do j=2,ny-1
            yj = c + (j-1)*h 
            f = f + g(x, yj)
        end do

        f = h*f
    end function f

end module functions
